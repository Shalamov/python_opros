# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from lxml import etree


class RepositoryTest(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def set_connect_data(self, connect_data):
        pass

    @abstractmethod
    def set_creator_structure_test(self, creator_structure_test):
        pass

    @abstractmethod
    def load_test_for_id(self, id_test):
        pass

    @abstractmethod
    def get_next_question(self,):
        pass

    @abstractmethod
    def get_true_answer_for_question(self, question, answer):
        pass

    @abstractmethod
    def get_name_test(self):
        pass


class XmlTest(RepositoryTest):
    def set_connect_data(self, connect_data):
        self.connect_data = connect_data
        self.node_test = etree.parse(self.connect_data).findall("test")

    def set_creator_structure_test(self, creator_structure_test):
        self.creator_structure_test = creator_structure_test

    def get_all_test(self):
        try:
            return [[i.find("id_test").text, i.find("name").text] for i in self.node_test]
        except AttributeError:
            raise AttributeError('Invalid file format')

    def load_test_for_id(self, id_test):
        try:
            for test in self.node_test:
                if test.find("id_test").text == str(id_test):
                    self.test_structure = self.creator_structure_test(test).create_structure_test()
                    self.name_test = test.find("name").text
                    return True
        except AttributeError:
            raise AttributeError('Invalid file format')

        raise IndexError('not tests for this id')

    def get_next_question(self,):
        for quest in self.test_structure:
            yield quest

    def get_true_answer_for_question(self, question, answer):
        ans_list = question[1]
        for ans in ans_list:
            if ans['id_answer'] in answer and ans['correct'] == 'True':
                pass
            if ans['id_answer'] in answer and ans['correct'] == 'False':
                return False
            if ans['correct'] == 'True' and ans['id_answer'] not in answer:
                return False
        return True

    def get_name_test(self):
        return self.name_test

