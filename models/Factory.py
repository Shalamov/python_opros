# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod

from Test import XmlTest
from PassTest import SimplePassTest
from settings import settings
from StructureTreeTest import StructureTestXml


class AbstractFactory(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_object(self,):
        pass


class TestFactory(AbstractFactory):
    objects = {
        'xml': [XmlTest, settings.PATH_TO_XML_DB, StructureTestXml],
        'default': [XmlTest, settings.PATH_TO_XML_DB, StructureTestXml],
    }

    def get_object(self, name="default"):

        obj = self.objects[name]
        object = obj[0]()
        object.set_connect_data(obj[1])
        object.set_creator_structure_test(obj[2])
        return object


class PassTestFactory(AbstractFactory):
    objects = {
        'xml': SimplePassTest,
        'default': SimplePassTest,
    }

    def get_object(self, name="default"):
        return self.objects[name]()
