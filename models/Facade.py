# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from models.Factory import PassTestFactory, TestFactory


class AbstractFacade(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, type_test):
        pass

    @abstractmethod
    def get_all_tests(self):
        pass

    @abstractmethod
    def get_test_for_id(self, id_test):
        pass

    @abstractmethod
    def get_question(self):
        pass


class Facade(AbstractFacade):
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Facade, cls).__new__(cls, *args, **kwargs)
        return cls.instance

    def __init__(self, type_test='xml'):
        self.test = TestFactory().get_object(type_test)
        self.pass_test = PassTestFactory().get_object(type_test)

    def get_all_tests(self):
        return self.test.get_all_test()

    def get_test_for_id(self, id_test):
        if self.test.load_test_for_id(id_test):
            self.pass_test.set_test(self.test)
            return self.pass_test.begin_test()

    def get_question(self):
        return self.pass_test.step()


if __name__ == '__main__':
    f = Facade()
    f1 = Facade()

    print f == f1