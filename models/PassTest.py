# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod


class AbstractPassTest(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def set_test(self, test):
        pass

    @abstractmethod
    def begin_test(self,):
        pass

    @abstractmethod
    def step(self,):
        pass

    @abstractmethod
    def result_for_question(self, quest, answer):
        pass

    @abstractmethod
    def result_for_test(self):
        pass


class SimplePassTest(AbstractPassTest):
    def set_test(self, test):
        self.test = test
        self.quest = test.get_next_question()
        self.count_incorrect_answer = 0

    def get_quests(self):
        return self.quest

    def begin_test(self,):
        return self.test.get_name_test()

    def step(self,):
        quest = self.quest.next()
        return quest

    def result_for_question(self, quest, answer):
        if self.test.get_true_answer_for_question(quest, answer):
            return True
        else:
            self.count_incorrect_answer += 1
            return False

    def result_for_test(self):
        return self.count_incorrect_answer
