# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod


class AbstractStructureTest(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, test):
        pass

    @abstractmethod
    def create_structure_test(self):
        pass

    @abstractmethod
    def get_test_name(self):
        pass


class StructureTestXml(AbstractStructureTest):
    def __init__(self, test):
        self.test = test
        self.questions = self.test.findall('question')

    def create_structure_test(self):
        quests = []
        for quest in self.questions:
            quests.append(QuestionNodeXml(quest).get_quest_node())

        return quests

    def get_test_name(self):
        try:
            return self.test.find("name").text
        except AttributeError:
            raise AttributeError('Invalid file format')


class QuestionNodeXml(object):
    def __init__(self, quest):
        self.quest = quest

    def get_quest(self):
        try:
            return self.quest.find("quest").text
        except AttributeError:
            raise AttributeError('Invalid file format')

    def get_answer_structure(self, answer):
        return {'id_answer': answer.find("id_answer").text, 'text': answer.find("text").text, 'correct': answer.find("correct").text}

    def get_answers(self):
        result = []
        for ans in self.quest.findall("answer"):
            result.append(self.get_answer_structure(ans))

        return result

    def get_quest_node(self):
        return [self.get_quest(), self.get_answers()]
