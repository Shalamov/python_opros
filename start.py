# -*- coding: utf-8 -*-
from models.Facade import Facade


def main():
    f = Facade()
    print u"Список тестов"
    for test in f.get_all_tests():
        print "id = " + test[0]
    print u"Введите id теста для его прохождения:"
    id_test = input()
    print u"Вы проходите тест " + f.get_test_for_id(id_test)
    for quest in f.pass_test.quest:
        print u"Вопрос: " + quest[0]
        for ans in quest[1]:
            print ans['id_answer'] + ") " + ans['text']
        print u"Введите через запятую номера ответов:"
        answer_user = raw_input().split(',')
        f.pass_test.result_for_question(quest, answer_user)

    print u"Вы дали неправильных ответов: {0}".format(f.pass_test.result_for_test())


if __name__ == '__main__':
    main()
