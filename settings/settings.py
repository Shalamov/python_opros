# -*- coding: utf-8 -*-
import os

PROJECT_PATH_ROT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))

PATH_TO_XML_DB = os.path.join(PROJECT_PATH_ROT, 'db', 'db.xml')